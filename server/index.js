const express = require('express');
const cors = require('cors');
const path = require('path')
const app = express();

const PORT = 4000;

app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(express.static(path.join(__dirname, 'public')));

app.post("/login", (req, res) => {
    const { code } = req.body.params;
    
    // console.log(code);
    if(!code){

        res.sendStatus(500)

    } else {
        const authCode = "22";
        if(code === authCode){
            // req.session.user = "admin";
            res.sendStatus(200);

        } else{
            res.sendStatus(401);
        }
    }
});

app.get('/logout', function(req, res){
    req.session.destroy(function(){
       console.log("user logged out.")
    });
    res.redirect('/login');
 });


 //GET DETAILS ABOUT THE TRIP
 app.get('/tripDetails/details', (req, res) => {

    const serverPath = "https://vercelapp-server.vercel.app/";

    const city = {
        photos: [
            serverPath + 'images/city/img1.jpg',
            serverPath + 'images/city/img2.jpg',
            serverPath + 'images/city/img3.jpg',
            serverPath + 'images/city/img4.jpg',
            serverPath + 'images/city/img5.jpg',
            serverPath + 'images/city/img6.jpg',
            serverPath + 'images/city/img7.jpg',
            serverPath + 'images/city/img8.jpg',
            serverPath + 'images/city/img9.jpg',

        ]
    }

    const planningDetails = {
        map: serverPath + 'images/planning/cityMap.png',
        days: [
            {
                dayNumber: 1,
                day: "29/02",
                events: [
                    {
                        name: "Arrivo a Roma",
                        startTime: "11:00"
                    },
                    {
                        name: "Arrivo in Hotel",
                        startTime: "11:20",
                        duration: '10'
                    },
                    {
                        name: "Fontana di Trevi"
                    },
                    {
                        name: "Check-In Hotel",
                        startTime: "14:00",
                    },
                    {
                        name: "Colosseo e Foro Romano",
                        duration: '3h'
                    },
                    {
                        name: "Arco di Costantino",
                    }
                ]
            },
            {
                dayNumber: 2,
                day: "01/03",
                events: [
                    {
                        name: "Castel Sant'Angelo",
                        startTime: "11:00"
                    },
                    {
                        name: "Musei Vaticani",
                        startTime: "13:00",
                        duration: '2/3h'
                    },
                    {
                        name: "Basilica di San Pietro",
                        duration: "1h"
                    },
                    {
                        name: "Ristorante Il Ciociaro",
                        startTime: "21:00"
                    }
                ]
            },
            {
                dayNumber: 3,
                day: "02/03",
                events: [
                    {
                        name: "Pantheon",
                    },
                    {
                        name: "Piazza Navona",
                    },
                    {
                        name: "Pranzo da Osteria Da Fortunata (?)",
                    },
                    {
                        name: "Trastevere",
                    },
                    {
                        name: "Belvedere del Gianicolo",
                    }
                ]
            },
            {
                dayNumber: 4,
                day: "03/03",
                events: [
                    {
                        name: "Piazza di Spagna",
                    },
                    {
                        name: "Piazza del Popolo",
                    },
                    {
                        name: "Villa Borghese (?)",
                    },
                    {
                        name: "Arrivo in stazione",
                        startTime: "19:10"
                    }
                ]
            }
        ]
    }

    const ticketsDetails = {
        "outward": {
            "day": "29 Febbraio 2024",
            "leaveTime": "08:52",
            "arrivalTime": "11:00",
            "company": "italo",
            "ticket": serverPath + 'tickets/andata.pdf'
        },
        "return": {
            "day": "03 Marzo 2024",
            "leaveTime": "19:25",
            "arrivalTime": "21:48",
            "company": "trenitalia",
            "ticket": serverPath + "tickets/ritorno.pdf"
        }

    }

    const hotelDetails = {
        "hotelName": "La Foresteria Luxury Suite",
        "checkIn" : "14:00 - 20:00",
        "checkOut": "08:30 - 10:00",
        "roomType": "King Room with Balcony",
        "photos": [
            serverPath + 'images/hotel/img1.jpg',
            serverPath + 'images/hotel/img2.jpg',
            serverPath + 'images/hotel/img3.jpg',
            serverPath + 'images/hotel/img4.jpg',
        ]
    };

    const tripDetails = {
            city,
            ticketsDetails,
            hotelDetails,
            planningDetails
        
    }
    res.json(tripDetails);
 });

app.listen(PORT, console.log("Server has started on 4000"));