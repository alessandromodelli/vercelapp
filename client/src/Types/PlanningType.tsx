export type PlanningType = {
    map: string,
    days: DailyPlanningType[]
}

export type DailyPlanningType = {
    dayNumber: number,
    day: string
    events: DailyEventPlanningType[]

}

export type DailyEventPlanningType = {
    name: string
    startTime?: string
    duration?: string
}