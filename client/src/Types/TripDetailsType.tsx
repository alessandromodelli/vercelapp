import { CityType } from "./CityType"
import { HotelType } from "./HotelType"
import { PlanningType } from "./PlanningType"
import { TicketsType } from "./TicketsType"

export type TripDetailsType = {
    city: CityType,
    ticketsDetails: TicketsType,
    hotelDetails: HotelType
    planningDetails: PlanningType
}