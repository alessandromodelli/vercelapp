export type TicketsType = {
    outward: {
        day: string
        leaveTime: string,
        arrivalTime: string,
        company: string, 
        ticket: string
    },
    return: {
        day: string
        leaveTime: string,
        arrivalTime: string,
        company: string, 
        ticket: string
    }
}