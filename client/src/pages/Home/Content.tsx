import classes from './Content.module.css'
import Heart from '../../components/Heart/Content'
import Lock from '../../components/Lock/Content'

import { useState } from 'react'
import Login from '../../components/Login/Content'
import Riddle from '../../components/Riddle/Content'

const Home = () => {

    const [showLogin, setShowLogin] = useState(false)


    const handleShowLogin = () => {
        setShowLogin(!showLogin)
    }

    return (

        <div className={classes.home_main_container}>
            <Heart children={<Lock onClick={handleShowLogin} />} />
            <Riddle />



            {showLogin && <Login handleShowLogin={handleShowLogin} />}

        </div>


    )
}

export default Home
