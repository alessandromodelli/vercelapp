import Typography from '@mui/material/Typography';
import classes from './Content.module.css'
import { Link, useNavigate } from 'react-router-dom'
import Heart from '../../components/Heart/Content';

const Surprise = () => {
    const navigate = useNavigate();
  return (
    <div className={classes.surprise_rootContainer}>
        <div className={classes.surprise_infoContainer}>
        <Typography ><strong>Proprio così pulce!</strong><br/> Nessuno possiede le chiavi del mio cuore a parte <Typography variant='body1'>TE</Typography><br/>Per questo motivo ho deciso di farti una delle mie sorprese...</Typography>
        <Typography ><strong>PS: </strong>Nessuno mi tocca il cuoricino come lo fai tu</Typography>
      <Link to="/details"><Heart /></Link>

        </div>

    </div>
  )
}

export default Surprise
