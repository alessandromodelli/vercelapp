import Card from '../../components/Card/Content'
import Img from '../../components/Img/Content'
import classes from './Content.module.css'
import RomeImg from '../../assets/img/rome.jpg'
import TreviImg from '../../assets/img/trevi.jpg'
import TicketSummary from '../../components/TicketsSummary/Content'
import Carousel from '../../components/Carousel/Content'
import HotelSummary from '../../components/HotelSummary/Content'
import { TripDetailsType } from '../../Types/TripDetailsType'
import axios from 'axios'
import { SetStateAction, useEffect, useState } from 'react'
import PlanningSummary from '../../components/PlanningSummary/Content'
import Loader from '../../components/Loader/Content'
import mailImg from '../../assets/img/mail.svg'
import Mail from '../../components/Mail/Content'
import GalleryView from '../../components/GalleryView/Content'
import { useAuth } from '../../components/Hooks/useAuth'
import Button from '../../components/Button/Content'

const Details = () => {

  const [results, setResults] = useState<TripDetailsType>();
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | undefined>();

  const [showDetails, setShowDetails] = useState<boolean>(false);

  const { logout } = useAuth();

  const getDetails = () => {
    setLoading(true);
    axios.get("https://vercelapp-server.vercel.app/tripDetails/details")
      .then((response) => {

        setResults(response.data)
        setLoading(false);
        console.log(response);
      })
      .catch((error) => {
        setError("Errore richiesta");
        console.log(error)
      })
  }

  useEffect(() => {
    getDetails()
  }, [])

  // useEffect(() => {
  //   console.log(results)
  // }, [results])

  const imgs = [RomeImg, TreviImg, RomeImg];

  const [showGallery, setShowGallery] = useState(false);

  const [activeSlide, setActiveSlide] = useState<number>(0);

  console.log("Details", activeSlide)

  const [isCarouselLoaded, setIsCarouselLoaded] = useState(false);

  return (
    <div className={classes.detail_root_container}>

      {
        loading ? <Loader className={classes.loaderContainer} />
          :

          showDetails ?

            results && (
              <div className={classes.divInfo}>
                <Carousel slides={results.city.photos} openGallery={() => { setShowGallery(!showGallery) }} activeSlide={activeSlide} setActiveSlide={setActiveSlide} onLoad={() => setIsCarouselLoaded(true)} />
                {
                  isCarouselLoaded && <>
                    <TicketSummary tickets={results.ticketsDetails} />
                    <HotelSummary hotel={results.hotelDetails} />
                    <PlanningSummary planning={results.planningDetails} />
                    <GalleryView
                      isOpen={showGallery} setIsOpen={() => setShowGallery(!showGallery)} activeSlide={activeSlide} setActiveSlide={setActiveSlide} immagini={results?.city.photos} />
                  </>
                }

                <Button onClick={logout}>ESCI</Button>
              </div>
            )

            :

            <Mail onClick={() => setShowDetails(!showDetails)} />
      }

      {/* {results?.city.photos && <Carousel slides={results.city.photos} />}
      {results?.ticketsDetails && <TicketSummary tickets={results?.ticketsDetails}/>}
      {results?.hotelDetails && <HotelSummary hotel={results.hotelDetails} />}
      {results?.planningDetails && <PlanningSummary planning={results.planningDetails}/>} */}


    </div>
  )
}

export default Details
