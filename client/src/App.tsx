

import Layout from './components/Layout/Content';
import Home from './pages/Home/Content';
import Details from './pages/Details/Content'

import { Routes, Route } from 'react-router-dom';
import Surprise from './pages/Surprise/Content';
import { useAuth } from './components/Hooks/useAuth';
import { useEffect } from 'react';
import Routing from './components/Routing/Routing';

function App() {

  const { isLoggedIn } = useAuth();

  // const setRoutes = () => {
  //   if (isLoggedIn) {
  //     return (
  //       <Routes>
  //         <Route path="/" element={<Layout title="22"><Surprise /></Layout>} />
  //         <Route path="/surprise" element={<Layout title="22"><Surprise /></Layout>} />
  //         <Route path="/details" element={<Layout title="Destinazione 2024"><Details /></Layout>} />
  //         <Route path="*" element={<Layout title="22"><Surprise /></Layout>} />
  //       </Routes>
  //     )
  //   } else {
  //     return (

  //     )
  //   }
  // }

  return (
    <Routing />
  );
}

export default App;
