import React from 'react'
import classes from './Content.module.css'
import TicketDetails from '../TicketDetails/Content'
import LogoTrenitalia from '../../assets/img/logoTrenitalia.svg'
import LogoItalo from '../../assets/img/logoItalo.svg'
import Card from '../Card/Content'
import { Typography } from '@mui/material'
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { TicketsType } from '../../Types/TicketsType'
import CommuteIcon from '@mui/icons-material/Commute';

type TicketSummaryType = {
    tickets?: TicketsType;
}

const TicketSummary = ({tickets} : TicketSummaryType) => {
    return (
        <Card className={classes.mainCard}>
        <div className={classes.ticket_rootContainer}>
            <Typography variant={'h3'}>Viaggio</Typography> 
            <div className={classes.infoRouteContainer}>
            <Typography variant={'h6'}>Andata</Typography> <ArrowForwardIcon />
            </div>
            
            <Card>
                <TicketDetails title={tickets?.outward.day ?? ""} leaveTime={tickets?.outward.leaveTime ?? ""} arrivalTime={tickets?.outward.arrivalTime ?? ""} img={LogoItalo} ticketUrl={tickets?.outward.ticket ?? ""} />
            </Card>


            {/* <div className={classes.hr} /> */}
            <div className={classes.infoRouteContainer}>
            <Typography variant={'h6'}>Ritorno</Typography> <ArrowBackIcon />
            </div>
            <Card>
            <TicketDetails title={tickets?.return.day ?? ""} leaveTime={tickets?.return.leaveTime ?? ""} arrivalTime={tickets?.return.arrivalTime ?? ""} img={LogoTrenitalia} ticketUrl={tickets?.return.ticket ?? ""} />
            </Card>
        </div>
        </Card>
    )
}

export default TicketSummary
