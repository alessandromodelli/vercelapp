import Typography from '@mui/material/Typography'
import { DailyEventPlanningType, DailyPlanningType } from '../../Types/PlanningType'
import Card from '../Card/Content'
import classes from './Content.module.css'
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';

type EventsPlanninType = {
    events: DailyEventPlanningType[]
}

const EventsPlanning = ({events} : EventsPlanninType) => {
    return (
        <Card>
            <div className={classes.dailyInfoContainer}>
                <div className={classes.timeTable}>
                    {
                        events && events.map((event, index) => {
                            return (
                                <Typography>{event.startTime ?? "-"}</Typography>
                            )
                        })
                    }
                </div>
                <div className={classes.timeBar} />
                <div>
                    {
                        events && events.map((event, index) => {
                            return (
                                <div>
                                <Typography> {event.name}</Typography>
                                {index !== events.length-1 && <ArrowDownwardIcon style={{margin: "auto"}}/>}
                                
                                </div>
                            )
                        })
                    }
                </div>

            </div>


        </Card>
    )
}

export default EventsPlanning
