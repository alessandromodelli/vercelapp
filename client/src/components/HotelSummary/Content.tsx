import React, { useState } from 'react'
import classes from './Content.module.css'
import Typography from '@mui/material/Typography'
import Card from '../Card/Content'
import Carousel from '../Carousel/Content'
import { HotelType } from '../../Types/HotelType'
import GalleryView from '../GalleryView/Content'

type HotelProps = {
    hotel: HotelType;
}

const HotelSummary = ({ hotel }: HotelProps) => {

    const [showGallery, setShowGallery] = useState(false);
    const [activeSlide, setActiveSlide] = useState(0);

    return (
        <Card>
            <div className={classes.hotel_rootContainer}>
                <Typography variant={'h3'}>Hotel</Typography>

                {/* <Card> */}
                <div className={classes.infoHotelContainer}>

                    <Typography variant={'h5'} fontWeight={700}>{hotel.hotelName ?? ""}</Typography>

                    <Typography variant={'h6'}><strong>Camera:</strong> {hotel.roomType ?? ""}</Typography>
                    <div>
                        <div className={classes.checkInOutContainer}>
                            <Card>
                                <div className={classes.checkInOutBox}>
                                    <Typography variant={'h6'} fontWeight={700}>Check-In:</Typography>
                                    <Typography variant={'h6'}>{hotel.checkIn ?? ""}</Typography>
                                </div>
                            </Card>
                            <Card>
                                <div className={classes.checkInOutBox}>
                                    <Typography variant={'h6'} fontWeight={700}>Check-Out:</Typography>
                                    <Typography variant={'h6'}> {hotel.checkOut ?? ""}</Typography>
                                </div>
                            </Card>
                        </div>
                    </div>
                </div>
                {/* </Card> */}

                <div className={classes.carousel}>
                    <Carousel slides={hotel.photos} swiperClassName={classes.swiperSlide} openGallery={() => setShowGallery(!showGallery)} setActiveSlide={setActiveSlide}/>
                </div>
                {/* <div className={classes.carousel}>
                    <Carousel slides={hotel.photos} swiperClassName={classes.swiperSlide}  setActiveSlide={setActiveSlide}/>
                </div> */}

                <GalleryView 
              isOpen={showGallery} setIsOpen={() => setShowGallery(!showGallery)} activeSlide={activeSlide} setActiveSlide={setActiveSlide} immagini={hotel.photos} />




            </div>
        </Card>
    )
}

export default HotelSummary
