import classes from './Content.module.css'
import { ReactNode } from "react";

type ButtonProps = {
    className?: string,
    disabled?: boolean,
    onClick?: () => void,
    children?: ReactNode,
    type?: "submit" | "reset" | "button"
}

const Button = ({
  className,
  disabled,
  type,
  onClick,
  children

} : ButtonProps) => {


  return (
    <button
    disabled={disabled ? disabled : false}
    type={type ? type : "button"}
    className={className ? className : classes.button}
    onClick={onClick}

  >
    {children}
  </button>
  );
};

export default Button;

