import React, { ReactNode } from 'react'
import classes from './Content.module.css'

type CardProps = {
    children?: ReactNode
    className?: string
}
const Card = ({children, className}: CardProps) => {
  return (
    <div className={className ? [classes.card_container,className].join(" ") : classes.card_container}>
      {children}
    </div>
  )
}

export default Card
