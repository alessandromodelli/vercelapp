import classes from './Content.module.css'
import axios from 'axios'
import CloseIcon from '@mui/icons-material/Close';
import Input from '../Input/Content';
import LockIcon from '@mui/icons-material/Lock';
import Button from '../Button/Content';
import KeyPad from '../KeyPad/Content';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Loader from '../Loader/Content';
import { useAuth } from '../Hooks/useAuth';

type LoginProps = {
    handleShowLogin: () => void
}

const Login = ({ handleShowLogin }: LoginProps) => {


    const { setToken } = useAuth();

    const [pin, setPin] = useState<string>("");
    const [error, setError] = useState<string>("");
    const [status, setStatus ] = useState<number | undefined>(undefined);
    const [loading, setLoading] = useState<boolean>(false)

    const navigate = useNavigate()

    const handlePin = (value: string) => {
        if (pin.length < 2) {
            setPin(pin + value)
        } else {
            setError("Lunghezza massima 2 caratteri")
        }

    }

    const handleDelete = () => {
        if (pin.length === 2) {
            setError("")
        }
        setPin(pin.slice(0, -1))
        setStatus(undefined)

    }

    const handleOnChange = () => {
        console.log(pin)
    }



    const login = () => {
        setLoading(true);
        axios.post("https://vercelapp-server.vercel.app/login", { params: {
            code: pin
        }

        })
            .then((response) => {
                setLoading(false);
                // console.log(response)
                if(response.status === 200){
                    setToken("150224")
                    navigate("/surprise")
                }
            })
            .catch((error) => {
                
                setStatus(error.response.status);
                setLoading(false);
                // console.log("this is the error",error.response)
            })
    }

    return (
        <div className={classes.login_main_container} >
            <div className={classes.login_header_container}>
                <CloseIcon onClick={handleShowLogin} className={classes.closeIcon} />
            </div>
            <Input icon={<LockIcon />} value={pin} onChange={() => handleOnChange} handleDelete={handleDelete} error={error} status={status}/>
            {loading && <Loader />}
            <KeyPad numberOfDigits={9} handlePin={handlePin} />
            <Button onClick={login} >ENTRA</Button>
        </div>
    )
}

export default Login
