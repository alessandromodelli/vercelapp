import { Typography } from '@mui/material'
import classes from './Content.module.css'
import Card from '../Card/Content'

const Riddle = () => {
  return (
    <div className={classes.riddle_rootContainer}>
      {/* <Card>
        <div className={classes.textContainer}> */}
        <Typography variant={"caption"} className={classes.riddleText}>
            Oggi <strong>Amore</strong> è un giorno molto speciale... è il tuo <strong>22esimo </strong>compleanno! 
            Esattamente il <strong>secondo</strong> che trascorriamo insieme.
            L'<strong>anno</strong> in cui sei entrata a far parte della mia vita è, però, altrettanto <strong>importante</strong>, in particolare gli ultimi <strong>2</strong> mesi.
            Sembra quasi incredibile come tutto ruoti attorno a <strong>due singoli numeri</strong>...<br/> 
            <Typography><strong>Due numeri che ti permettono di avere le chiavi del mio cuore!</strong></Typography>

        </Typography>
        {/* </div> */}

    {/* //   </Card> */}

    </div>
  )
}

export default Riddle
