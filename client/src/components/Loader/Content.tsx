// import TextComponent from '../TextComponent'
import classes from './Content.module.css'

type Props = {
  size?: number,
  percentage?: string,
  message?: string,
  className?: string
}

const Loader = ({ size, className}: Props) => {

//   const top = `calc(50vh - ${(size ?? 75) /2}px)`
//   const left = `calc(50% - ${(size ?? 75) /2}px)`

  return (
    <div className={className ? className : classes.loading_overlay}>
      <div className={classes.loader} style={{
        height: size ?? 75,
        width: size ?? 75
      }}></div>
    </div>
  )
}

export default Loader
