import { Typography } from '@mui/material';
import classes from './Content.module.css'
import { ReactNode } from "react";
// import PinButton from '../PinButton/Content';
import BackspaceIcon from '@mui/icons-material/Backspace';
import Error from '../Error/Content'

type InputProps = {
    className?: string,
    containerStyle?: string,
    errors?: string,
    disabled?: boolean,
    icon?: ReactNode,
    inputStyle?: string,
    name?: string,
    onChange?: () => {},
    placeholder?: string,
    required?: boolean,
    type?: string,
    value?: string,
    wrapperStyle?: string
    inputRef?: React.LegacyRef<HTMLInputElement>
    handleDelete: () => void
    error?: string
    status?: number | undefined
}

const Input = ({
  className,
  containerStyle,
  errors,
  disabled,
  icon,
  inputStyle,
  name,
  onChange,
  status,
  placeholder,
  required,
  type,
  value,
  wrapperStyle,
  inputRef,
  handleDelete,
  error
} : InputProps) => {

  const handleStatus = () => {
    if(status && status !== 200){
        return(
          <p style={{color: "red"}}>RIPROVA</p>
        )
    }
}

  return (
    <div className={className ? className : classes.input_main_container}>
      <div className={containerStyle ? containerStyle : classes.input_container}>
        {icon ? icon : <p>c</p>}
        {/* <input
          ref={inputRef}
          aria-label={name}
          data-testid={name}
          tabIndex={0}
          type={type}
          name={name}
          onChange={onChange}
          placeholder={placeholder}
          value={value}
          className={classes.input}
          disabled={disabled}
        /> */}
        <Typography>{value}</Typography>
        {/* <PinButton >CANC</PinButton> */}
        <BackspaceIcon className={classes.backspace_icon} onClick={handleDelete}/>
      </div>
      {errors && !value && (
        <p>Required!</p>
      )}
      {error && (
        <Error error={error}/>
      )}
      {
        handleStatus()
      }
    </div>
  );
};

export default Input;

