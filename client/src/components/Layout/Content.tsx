import { ReactNode } from 'react'
import classes from './Content.module.css'
import Header from '../Header/Content'

type LayoutProps = {
  title: string
  children: ReactNode
  isHome?: boolean;
}

const Layout = ({ title, children, isHome = false }: LayoutProps) => {
  return (
    <div className={isHome ? classes.rootContainerHome : classes.rootContainer}>
      {isHome ? <Header description={true}>{title}</Header>
        : <Header >{title}</Header>}

      {children}
    </div>
  )
}

export default Layout
