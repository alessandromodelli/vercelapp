import React, { Dispatch, SetStateAction, useEffect, useState } from 'react'

import { Navigation, Pagination, Scrollbar, A11y } from 'swiper/modules';

import { Swiper, SwiperClass, SwiperSlide } from 'swiper/react';
import classes from './Content.module.css'

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/scrollbar';

import CarouselCard from '../CarouselCard/Content';

type CarouselProps = {
    slides: string[],
    slidesInCard?: number
    swiperClassName?: string
    showNavigation?: boolean
    activeSlide?: number
    setActiveSlide: Dispatch<SetStateAction<number>>
    onSlideChange?: (swiper: SwiperClass) => void;
    openGallery?: () => void
    onLoad?: () => void
}

const Carousel = ({slides, slidesInCard = 1, swiperClassName, showNavigation = false, activeSlide, onSlideChange, openGallery, setActiveSlide, onLoad}:CarouselProps) => {

//   const [swiper, setSwiper] = useState<SwiperClass>();

//   useEffect(() => {

//       swiper?.slideTo(3)
    
// },[swiper])

  return (
    <div className={classes.swiperContainer} onClick={openGallery} onLoad={onLoad}>
          <Swiper
        className={swiperClassName ? [classes.swiper, swiperClassName].join(" ") : classes.swiper}
        initialSlide={activeSlide ? activeSlide : 0}

      // install Swiper modules
      modules={[Navigation, Pagination, Scrollbar, A11y]}
      spaceBetween={50}
      slidesPerView={1}
      navigation={showNavigation}
      pagination={{ clickable: true}}
      onSwiper={(swiper: any) => console.log(swiper)}
      onSlideChange={(swiper: any) => {setActiveSlide(swiper.activeIndex)}}
      style={{
        "--swiper-pagination-color": "rgb(255, 131, 131)",
        "--swiper-pagination-bullet-inactive-color": "#999999",
        "--swiper-pagination-bullet-inactive-opacity": "0.7",
        "--swiper-pagination-bullet-size": "10px",
        "--swiper-pagination-bullet-horizontal-gap": "6px"
      }}
    >
      {slides.map((card, index) => {
            const cardSlides: string[] = [];
            if (index % slidesInCard === 0) {
              cardSlides.push(...slides.slice(index, index + slidesInCard))
            } else {
              return;
            }

            return (
              <SwiperSlide key={"slide-" + index} className={classes.swiper_slide}>
                       <CarouselCard  key={card} imgUrl={card} />
              </SwiperSlide>
            )
          })}
      
    </Swiper>
    </div>
  )
}

export default Carousel
