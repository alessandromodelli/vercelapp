import classes from './Content.module.css'
import { ReactNode } from "react";

type PinButtonProps = {
    className?: string,
    disabled?: boolean,
    onClick: (value: string) => void,
    children?: ReactNode,
    type?: "submit" | "reset" | "button",
    digitValue?: string
}

const PinButton = ({
  className,
  disabled,
  type,
  onClick,
  children,
  digitValue

} : PinButtonProps) => {


  return (
    <button
    disabled={disabled ? disabled : false}
    type={type ? type : "button"}
    className={className ? className : classes.button}
    onClick={() => onClick(digitValue ?? "")}

  >
    {children}
  </button>
  );
};

export default PinButton;

