import React from 'react'
import classes from './Content.module.css'
import Img from '../Img/Content'
import { Typography } from '@mui/material'
import EmptyImg from '../../assets/img/noImg.png'
import { Link, useNavigate } from 'react-router-dom'

type TicketDetailsProps = {
    title: string,
    leaveTime: string,
    arrivalTime: string,
    img?: string,
    ticketUrl: string
}

const TicketDetails = ({title, leaveTime, arrivalTime, img, ticketUrl} : TicketDetailsProps) => {
    const navigate = useNavigate();
  return (
    <div className={classes.ticketDetails_rootContainer}>
        <div className={classes.header}>
            
            <Typography variant={'h6'}>{title}</Typography>
        </div>
        <div className={classes.details}>
            <div className={classes.timeTable}>
                <Typography>{leaveTime}</Typography>
                <div className={classes.hr} />
                <Typography>{arrivalTime}</Typography>
            </div>

            <Img src={img ? img : EmptyImg} alt={"img"} className={classes.trainLogo}/>

            {/* <button className={classes.button} onClick={() => navigate(ticketUrl)}></button> */}
            <Link className={classes.button} to={ticketUrl}>PDF</Link>
        </div>
    </div>
  )
}

export default TicketDetails
