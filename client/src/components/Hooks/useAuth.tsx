import { useContext } from "react";
import { AuthContextType, AuthContext } from "../AuthProvider/AuthProvider";

export const useAuth = (): AuthContextType => useContext(AuthContext);
