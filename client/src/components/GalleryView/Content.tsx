// import React, { useState } from 'react'
import { Dispatch, SetStateAction, useEffect, useState } from 'react';
import classes from './Content.module.css';
import { Typography } from '@mui/material';
import Carousel from '../Carousel/Content';
import CloseIcon from '@mui/icons-material/Close';
import { Navigation, Pagination, Scrollbar, A11y } from 'swiper/modules';

import { Swiper, SwiperClass, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/scrollbar';
import CarouselCard from '../CarouselCard/Content';

export type GalleryViewProps = {
    isOpen: boolean
    setIsOpen: () => void
    img?: string
    activeSlide?: number
    setActiveSlide: Dispatch<SetStateAction<number>>
    immagini: string[]
}


const GalleryView = ({ isOpen, setIsOpen, immagini, activeSlide, setActiveSlide }: GalleryViewProps) => {

    const [indexCarousel, setIndexCarousel] = useState(0);

    //useEffect()


    //TODO: CORRETTO???
    useEffect(() => {
        if (isOpen) {
            document.body.style.overflow = "hidden";
        } else {
            document.body.style.overflow = "auto";
        }
        setIndexCarousel(0)
    }, [isOpen])

    // useEffect(() => {
    //     console.log(indexCarousel, "index")

    // }, [indexCarousel])

    useEffect(() => {
        console.log('activeSlide',activeSlide)
    },[activeSlide])

    const imageOf = (indexCarousel + 1).toString() + " di " + (immagini.length).toString();

    const slidesInCard = 1;

    // const top = document.body.scrollTop;
    // console.log

    if (isOpen) {
        return (
            <div className={classes.main_container} style={{
                top: 0
            }}>
                {/* <NavigationTopBar className={classes.overlapping_topbar}
                    button_left={<IconTextButton
                        backColor="transparent"
                        contentsColor="white"
                        buttonMode="outline_borderless"
                        icon={<ChevronLeft height="24" strokeWidth={1.5} width="24" onClick={setIsOpen} />} />}
                    title_key={imageOf}
                    textColor="white"
                    button_right={<IconTextButton
                        backColor="transparent"
                        contentsColor="white"
                        buttonMode="outline_borderless"
                        icon={<X height="24" strokeWidth={1.5} width="24" onClick={setIsOpen

                        } />} />} /> */}
                
                <div className={classes.navBar}> 
                        <span></span>
                        <Typography>{imageOf}</Typography>
                        {/* <button onClick={setIsOpen}>X</button> */}
                        <CloseIcon onClick={setIsOpen} className={classes.closeIcon}/>
                </div>

                <div className={classes.carouselContainer}>
                    {/* <Carousel
                        //selectedSlide={activeSlide ? activeSlide.toString() : undefined}
                        initialSlide={activeSlide}
                        slides={immagini ?? []
                            }
                        // paddingType='none'
                        onSlideChange={(swiper) => {setIndexCarousel(swiper.activeIndex);}}
                        // setActiveSlide(swiper.activeIndex)
                    // selectedSlide="2"

                    /> */}
                    <Swiper
        className={classes.swiper}
        initialSlide={activeSlide ? activeSlide : 0}
      // install Swiper modules
      modules={[Navigation, Pagination, Scrollbar, A11y]}
      spaceBetween={50}
      slidesPerView={1}

    //   pagination={{ clickable: true}}
      onSwiper={(swiper: any) => console.log(swiper)}
      onSlideChange={(swiper: any) => {setActiveSlide(swiper.activeIndex); setIndexCarousel(swiper.activeIndex)}}
    >
      {immagini.map((card, index) => {
            const cardSlides: string[] = [];
            if (index % slidesInCard === 0) {
              cardSlides.push(...immagini.slice(index, index + slidesInCard))
            } else {
              return;
            }

            return (
              <SwiperSlide key={"slide-" + index} className={classes.swiper_slide}>
                       <CarouselCard  key={card} imgUrl={card} cardClassName={classes.carouselCard_img}/>
              </SwiperSlide>
            )
          })}
      
    </Swiper>
                </div>

            </div>
        );
    } else return <></>
}

export default GalleryView
