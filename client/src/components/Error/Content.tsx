import classes from './Content.module.css'

type ErrorProps = {
    error: string
}
const Error = ({error}: ErrorProps) => {
  return (
        <p className={classes.error}>{error}</p>
  )
}

export default Error
