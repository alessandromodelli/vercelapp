import React, { ReactNode, createContext, useEffect, useMemo, useState } from 'react'
import { useNavigate } from 'react-router-dom'

export type AuthContextType = {
    token: string, 
    setToken: React.Dispatch<React.SetStateAction<string>>
    isLoggedIn: boolean,
    ready: boolean
    logout: () => void

    // login: (code: string) => {}

}

export const AuthContext = createContext<AuthContextType>({} as AuthContextType);

type AuthProviderProps = {
    children: ReactNode | JSX.Element;
}


const AuthProvider = ({children}: AuthProviderProps) => {

    const [token, setToken] = useState<string>("");
    const [isLoggedIn, setIsLoggedIn] = useState<boolean>(false);
    const [ready, setReady] = useState<boolean>(false)

    const navigate = useNavigate();

    const logout = () => {
        localStorage.removeItem("token");
        setToken("")
        setIsLoggedIn(false)
        navigate("/");
    }   

    useEffect(() => {
        if(localStorage.getItem("token")){
            setToken(localStorage.getItem("token") ?? "");
            setIsLoggedIn(true)
        }
    }, [])

    
    useEffect(() => {
        console.log(token)
        if(token){
            localStorage.setItem("token", token)
            setIsLoggedIn(true);
        }
    }, [token, setToken]);



    const value: AuthContextType = {
            token,
            setToken,
            isLoggedIn,
            ready,
            logout
    }

    console.log(value)

  return (
    <AuthContext.Provider value={value}>
        {children}
    </AuthContext.Provider>
  )
}

export default AuthProvider
