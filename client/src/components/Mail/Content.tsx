import classes from './Content.module.css'
import MailImg from '../../assets/img/mail.svg'
import Pulce from '../../assets/img/Pulce.svg'

type MailProps = {
    onClick: () => void
}

const Mail = ({onClick} : MailProps) => {
  return (
    <div className={classes.mail_root_container}>
        <img src={MailImg} alt="" className={classes.mail} onClick={onClick}/>
        <div className={classes.shadow} />
        
      <img src={Pulce} alt="Pulce" className={classes.pulce} />

    </div>
  )
}

export default Mail
