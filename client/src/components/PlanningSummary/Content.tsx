import React, { useState } from 'react'
import classes from './Content.module.css'
import Typography from '@mui/material/Typography'
import { PlanningType } from '../../Types/PlanningType'
import Img from '../Img/Content'
import DailyPlanning from '../DailyPlanning/Content'
import Card from '../Card/Content'
import GalleryView from '../GalleryView/Content'

type PlanningSummaryType = {
  planning: PlanningType
}

const PlanningSummary = ({ planning }: PlanningSummaryType) => {

  const planningDays = planning.days;

  const [showGallery, setShowGallery] = useState(false);
  const [activeSlide, setActiveSlide] = useState(0);

  return (
    <Card>
      <div className={classes.planning_rootContainer}>
        <Typography variant={'h3'}>Programma</Typography>
        <Img src={planning.map} alt={'mappa'} className={classes.cityMap} onClick={() => setShowGallery(!showGallery)}/>
        <GalleryView
          isOpen={showGallery} setIsOpen={() => setShowGallery(!showGallery)} activeSlide={activeSlide} setActiveSlide={setActiveSlide} immagini={[planning.map]} />



        {planningDays && planningDays.map((day) => {
          return (
            <DailyPlanning key={"day" + day.dayNumber} dayNumber={day.dayNumber} day={day.day} events={day.events} />
          )
        })}

      </div>
    </Card>
  )
}

export default PlanningSummary
