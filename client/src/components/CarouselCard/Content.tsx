
import Skeleton from '@mui/material/Skeleton';
import Img from '../Img/Content';
import classes from './Content.module.css'
import { useState } from 'react';

export type CardType = 'mini' | 'smaller' | 'small' | 'medium' | 'large' | 'larger' | 'largetall' | 'largesquare' | 'fullwidth' | 'fullsize';

export interface CarouselCardProps {

  id?: string;

//   cardType: CardType;

  title?: string;
  titleColor?: string;
  titleWeight?: "medium" | "regular" | "bold" | "semibold" | undefined;
  /**
   * Text below the title for secondary information.
   */
  secondaryInfo?: string;
  secondaryInfoColor?: string;
  /**
   * Description of the card.
   */
  description?: string;
  descriptionColor?: string;
  /**
   * Color palette for all the text elements.
   */
  textColor?: 'light' | 'dark';
  /**
   * Image url.
   */
  imgUrl?: string;
  /**
   * Data for the button at the bottom of the card.
   */
  cardClassName?: string
}

const CarouselCard = ({
  title,
  titleColor = 'black',
  titleWeight,
  secondaryInfo,
  secondaryInfoColor = 'black',
  description,
  descriptionColor = 'black',
  textColor = 'dark',
  cardClassName,
  imgUrl
}: CarouselCardProps) => {

  const [imgLoaded, setImgLoaded] = useState(false);

  return (
    <div className={classes.carouselCard_container}>
        {/* <Img src={imgUrl ?? ""} alt={imgUrl ?? ""} className={classes.carouselCard_img} /> */}
        <img src={imgUrl} alt={imgUrl} className={imgLoaded ? cardClassName ? cardClassName : classes.carouselCard_img : [classes.image, classes.blur].join(" ")} onLoad={() => {setImgLoaded(true); console.log("loaded")}} style={{display: imgLoaded ? "" : "none"}}/>
        {!imgLoaded && <Skeleton height={400} className={classes.skeleton}/>}
        
    </div>
  );
};

export default CarouselCard;
