import Typography from '@mui/material/Typography';
import classes from './Content.module.css'
import { DailyPlanningType } from '../../Types/PlanningType';
// import EventsPlanning from '../EventsPlanning/Content';
import TodayIcon from '@mui/icons-material/Today';
import Card from '../Card/Content';
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import { useState } from 'react';



const DailyPlanning = ({ dayNumber, day, events }: DailyPlanningType) => {
    const [showDetails, setShowDetails ] = useState(false)

    const handleShowDetails = () => {
        setShowDetails(!showDetails);
    }
    return (
        <div className={classes.dailyPlanning_rootContainer}>
            
            <Card className={classes.card}>
            
            <div className={classes.dayTitle_container}>
            <Typography variant="h5" className={classes.dayLabel}>{day}<Typography variant='body1'> Giorno {dayNumber}</Typography></Typography>
            <button className={classes.showDetailsBtn} onClick={handleShowDetails}><Typography variant='caption'>{showDetails ? "Nascondi" : "Dettagli"}</Typography></button>
            </div>

            
            {showDetails && <div className={classes.dailyInfoContainer}>
            
            
            <div className={classes.timeTable}>
                {
                    events && events.map((event, index) => {
                        return (
                            <Typography key={"time" +  index}>{event.startTime ?? "-"}</Typography>
                        )
                    })
                }
            </div>
            <div className={classes.timeBar} />
            <div>
                {
                    events && events.map((event, index) => {
                        return (
                            <div key={"event"+ index}>
                            <Typography> {event.name}</Typography>
                            {index !== events.length-1 && <ArrowDownwardIcon style={{margin: "auto"}}/>}
                            
                            </div>
                        )
                    })
                }
            </div>

        </div>
            }
            


        </Card>
        </div>
    )
}

export default DailyPlanning;
