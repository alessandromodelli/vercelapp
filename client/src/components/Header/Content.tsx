import { ReactNode } from 'react'
import classes from './Content.module.css'
import Typography from '@mui/material/Typography'
import Riddle from '../Riddle/Content'

type HeaderProps = {
    children: ReactNode
    description?: boolean
}

const Header = ({children, description = false}: HeaderProps) => {
  return (
    <div className={classes.header_rootContainer}>
        <Typography variant="h3">{children}</Typography>
        {/* {description && <Riddle />} */}
    </div>
  )
}

export default Header
