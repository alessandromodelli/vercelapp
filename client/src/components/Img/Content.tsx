import { useState } from "react"
import classes from './Content.module.css'


type ImgProps = {
  src: string
  className?: string
  alt: string
  onClick?: () => void
}
const Img = ({ src, className, alt, onClick }: ImgProps) => {

  const [imgLoaded, setImgLoaded] = useState(false);

  return (
    <>
      {
        onClick ? <img src={src} alt={alt} className={imgLoaded ? className ? className : "" : [classes.image, classes.blur].join(" ")} onLoad={() => { setImgLoaded(true); console.log("loaded") }} onClick={onClick}/>
          :
          <img src={src} alt={alt} className={imgLoaded ? className ? className : "" : [classes.image, classes.blur].join(" ")} onLoad={() => { setImgLoaded(true); console.log("loaded") }} />

      }
    </>
      )

}

export default Img
