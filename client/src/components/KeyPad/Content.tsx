import PinButton from '../PinButton/Content'
import classes from './Content.module.css'

type KeyPadProps = {
    numberOfDigits: number,
    handlePin: (value: string) => void
}

const KeyPad = ({
    numberOfDigits,
    handlePin
}: KeyPadProps) => {


    return(
        <div className={classes.keypad_container}>
            {
              Array.from({length: numberOfDigits}, (_, key) => <PinButton key={"keypadKey" + key} onClick={handlePin} digitValue={`${key}`}>{key}</PinButton>)
              
            }
        </div>
    )
}

export default KeyPad;