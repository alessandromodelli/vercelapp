import { Routes, Route, useLocation } from "react-router-dom";
import ProtectedRoute, { ProtectedRouteProps } from "./ProtectedRoute";
import { useAuth } from "../Hooks/useAuth";
import Layout from "../Layout/Content";
import Home from "../../pages/Home/Content";
import Surprise from "../../pages/Surprise/Content";
import Details from "../../pages/Details/Content";
import { useEffect } from "react";

const Routing = () => {

    const { isLoggedIn } = useAuth();
    const location = useLocation();

    // console.log(isLoggedIn)

    const profileProtectedRouteProps: Omit<ProtectedRouteProps, 'component'> = {
        isAuthenticated: isLoggedIn,
        authenticationPath: "/",
    };

    // useEffect(() => {
    //     console.log(isLoggedIn)
    // }, [isLoggedIn])

    return (
        <>

            <Routes location={location} key={location.key} >

                {isLoggedIn ? <Route path="/" element={<Layout title="22"><Surprise /></Layout>} /> : <Route path="/" element={<Layout title="15 Febbrario 2024" isHome={true} ><Home /></Layout>} />}

                <Route path="/surprise" element={<ProtectedRoute {...profileProtectedRouteProps} component={<Layout title="22"><Surprise /></Layout>} />} />
                <Route path="/details" element={<ProtectedRoute {...profileProtectedRouteProps} component={<Layout title="Destinazione 2024"><Details /></Layout>} />} />
                <Route path="*" element={<ProtectedRoute {...profileProtectedRouteProps} component={<Layout title="22"><Surprise /></Layout>} />} />
{/* 
                {
                    isLoggedIn ?
                        <>
                            <Route path="/" element={<Layout title="22"><Surprise /></Layout>} />
                            <Route path="/surprise" element={<Layout title="22"><Surprise /></Layout>} />
                            <Route path="/details" element={<Layout title="Destinazione 2024"><Details /></Layout>} />
                            <Route path="*" element={<Layout title="22"><Surprise /></Layout>} />
                        </>
                        :
                        <>
                        <Route path="/" element={<Layout title="15 Febbrario 2024" isHome={true} ><Home /></Layout>} />
                        <Route path="*" element={<Layout title="15 Febbrario 2024" isHome={true} ><Home /></Layout>} />
                        </>
                } */}
            </Routes>


        </>


    );
};

export default Routing;
