import { Navigate } from "react-router-dom";

export type ProtectedRouteProps = {
    isAuthenticated: boolean;
    authenticationPath: string;
    component: JSX.Element;
};

const ProtectedRoute = (props: ProtectedRouteProps): JSX.Element => {
    console.log("Protected routes", props.isAuthenticated)
    if (props.isAuthenticated) {
        return props.component;
    } else {
        return <Navigate to={props.authenticationPath} />;
    }
};

export default ProtectedRoute;
